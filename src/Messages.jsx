import React, { Component } from 'react';

import Banner from './Banner';
import Threads from './Threads';

export default class Messages extends Component {
  state = {
    selectedThread: 0
  };

  selectThread = newSelectedThread => {
    this.setState({
      selectedThread: newSelectedThread
    });
  };

  render() {
    const threads = [
      {
        senderName: 'John Silver',
        id: '1',
        messages: [{ id: 1, body: 'hi buddy' }]
      },
      {
        senderName: 'Greg Goal',
        id: '2',
        messages: [{ body: 'yo yo' }, { id: 2, body: 'you good?' }]
      }
    ];

    const { selectedThread } = this.state;

    return (
      <section className="messages">
        {/* https://webaim.org/techniques/skipnav/ */}
        <a className="offscreen focusable" href="#messages">
          Skip to message inbox
        </a>
        <Banner heading="Messages" subHeading="1 new message" />
        {/* https://developer.mozilla.org/en-US/docs/Web/HTML/Element/main */}
        <main id="main-content">
          {/* https://developer.mozilla.org/en-US/docs/Web/HTML/Element/section */}
          <Threads threads={threads} onThreadClick={this.selectThread} />
          <section id="messages">
            <ol>
              {threads[selectedThread].messages.map(message => (
                <li key={message.id}>{message.body}</li>
              ))}
            </ol>
          </section>
        </main>
      </section>
    );
  }
}
