import React from 'react';

import './Banner.css';

const Banner = ({ heading, subHeading }) => (
  <>
    {/* https://developer.mozilla.org/en-US/docs/Web/HTML/Element/header header */}
    <header className="banner">
      <h1 className="banner__heading">{heading}</h1>
      {subHeading ? <p className="banner__subHeading">{subHeading}</p> : null}
    </header>
  </>
);

export default Banner;
