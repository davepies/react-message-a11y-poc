import React from 'react';

import './Threads.css';

const Threads = ({ threads, onThreadClick }) => (
  <section className="threads">
    <h1 className="offset" id="thread-header">
      Message Threads
    </h1>
    {/* https://developer.mozilla.org/en-US/docs/Web/HTML/Element/nav */}
    {/* roving tabindex https://www.w3.org/TR/wai-aria-practices/#kbd_roving_tabindex */}
    <nav aria-labelledby="thread-header" role="toolbar">
      <ol>
        {threads.map((thread, i) => (
          <li key={thread.id}>
            {/* maybe should not use it? http://www.heydonworks.com/article/aria-controls-is-poop */}
            <button
              onClick={() => onThreadClick(i)}
              type="button"
              aria-controls="messages"
            >
              {thread.senderName}
            </button>
          </li>
        ))}
      </ol>
    </nav>
  </section>
);

export default Threads;
